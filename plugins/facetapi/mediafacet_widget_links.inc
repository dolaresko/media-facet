<?php

/**
 * @file
 * The mediafacet_links and mediafacet_checkbox_links widget plugin classes.
 */

/**
 * Widget that renders facets as a list of clickable links.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup.
 */
class MediaFacetWidgetLinks extends FacetapiWidgetLinks {
  /**
   * Transforms the render array for use with theme_item_list().
   *
   * The recursion allows this function to act on the various levels of a
   * hierarchical data set.
   *
   * @param array $build
   *   The items in the facet's render array being transformed.
   *
   * @return array
   *   The "items" parameter for theme_item_list().
   */
  function buildListItems($build) {
    $settings = $this->settings->settings;

    // Initializes links attributes, adds rel="nofollow" if configured.
    $attributes = ($settings['nofollow']) ? array('rel' => 'nofollow') : array();
    $attributes += array('class' => $this->getItemClasses());

    // Builds rows.
    $items = array();

    if (!($colorbox_load = drupal_static('colorbox_load'))) {
      $colorbox_load = variable_get('colorbox_load', 0);
      // We enable colorbox load manually.
      if (!$colorbox_load) {
        drupal_add_js(drupal_get_path('module', 'colorbox') . '/js/colorbox_load.js');
        $colorbox_load = TRUE;
      }
    }

    if (!($image_tag = drupal_static('mediafacet_textimage_tag'))) {
      // We cache loaded image tag for future use.
      $image_tag = &drupal_static('mediafacet_textimage_tag');
      $image_tag = theme('image', array('path' => drupal_get_path('module', 'mediafacet') . '/mediafacet.png', 'width' => '16', 'height' => '16'));
    }

    if (!($mediaimage_tag = drupal_static('mediafacet_image_tag'))) {
      // We cache loaded image tag for future use.
      $mediaimage_tag = &drupal_static('mediafacet_image_tag');
      $mediaimage_tag = theme('image', array('path' => drupal_get_path('module', 'mediafacet') . '/mediafacet_image.png', 'width' => '16', 'height' => '16'));
    }
    $count = $this->facet->getAdapter()->getResultCount();
    foreach ($build as $value => $item) {
      $row = array('class' => array());

      // Initializes variables passed to theme hook.
      $variables = array(
        'text' => $item['#markup'],
        'path' => $item['#path'],
        'count' => $item['#count'],
        'options' => array(
          'attributes' => $attributes,
          'html' => $item['#html'],
          'query' => $item['#query'],
        ),
      );

      // Adds the facetapi-zero-results class to items that have no results.
      if (!$item['#count']) {
        $variables['options']['attributes']['class'][] = 'facetapi-zero-results';
      }

      // If the item has no children, it is a leaf.
      if (empty($item['#item_children'])) {
        $row['class'][] = 'leaf';
      }
      else {
        // If the item is active or the "show_expanded" setting is selected,
        // show this item as expanded so we see its children.
        if ($item['#active'] || !empty($settings['show_expanded'])) {
          $row['class'][] = 'expanded';
          $row['children'] = $this->buildListItems($item['#item_children']);
        }
        elseif(!empty($settings['show_expanded_narrow']) && $item['#count'] == $count) {
          $row['class'][] = 'expanded';
          $row['children'] = $this->buildListItems($item['#item_children']);
        }
        else {
          $row['class'][] = 'collapsed';
        }
      }

      // Gets theme hook, adds last minute classes.
      $class = ($item['#active']) ? 'facetapi-active' : 'facetapi-inactive';
      $variables['options']['attributes']['class'][] = $class;

      // Themes the link, adds row to items.
      $mediafacet_output = '';
      if (isset($item['#indexed_value']) && !empty ($item['#indexed_value'])) {
        $facet = $this->key . ':' . $item['#indexed_value'];
        $mediafacets = entity_load('mediafacet', FALSE, array('facet' => $facet));
        if (!empty($mediafacets)) {
          foreach ($mediafacets as $mediafacet) {
            if ($mediafacet->type == 'text') {
              $path = ($colorbox_load) ? 'mediafacet/' . $mediafacet->mfid . '/ajax' : 'mediafacet/' . $mediafacet->mfid . '/view';
              $mediafacet_attributes = ($colorbox_load) ? array('class' => 'mediafacet-link colorbox-load') : array('class' => 'mediafacet-link', 'target' => '_blank');
              $mediafacet_output .= l($image_tag, $path, array('query' => array('width' => '500'), 'attributes' => $mediafacet_attributes, 'html' => TRUE));
            }
            elseif ($mediafacet->type == 'audio' && isset($mediafacet->fid) && $file = file_load($mediafacet->fid)) {
              drupal_add_library('mediafacet', 'mediafacet_mediaelement');
              $mediafacet_output .= '<audio class="mediafacet-audio " audioWidth="16" audioHeight="16" src="' . file_create_url($file->uri) . '"></audio>';
            }
            elseif ($mediafacet->type == 'image' && isset($mediafacet->fid) && $file = file_load($mediafacet->fid)) {
              $path = file_create_url($file->uri);
              $mediafacet_attributes = ($colorbox_load) ? array('class' => 'mediafacet-image-link colorbox-load') : array('class' => 'mediafacet-image-link', 'target' => '_blank');
              $mediafacet_output .= l($mediaimage_tag, $path, array('query' => array('width' => '500'), 'attributes' => $mediafacet_attributes, 'html' => TRUE));
            }
          }
        }
      }
      $row['data'] = theme($item['#theme'], $variables) . ' ' . $mediafacet_output;
      $items[] = $row;
    }

    return $items;
  }

  /**
   * Gets the base class array for a facet item.
   *
   * Classes that extend FacetapiWidgetLinks will often overide this method to
   * alter the link displays via CSS without having to touch the render array.
   *
   * @return array
   *   An array of classes.
   */
  function getItemClasses() {
    return array();
  }

  /**
   * Overrides FacetapiWidget::settingsForm().
   */
  function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);
    if ($this->facet['hierarchy callback']) {
      $form['widget']['widget_settings']['links'][$this->id]['show_expanded_narrow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Expand hierarchy only when it does not narrow result'),
        '#default_value' => !empty($this->settings->settings['show_expanded_narrow']),
        '#description' => t('Works only when "Expand hierarchy" disabled.'),
        '#states' => array(
          'visible' => array(
            'select[name="widget"]' => array('value' => $this->id),
          ),
        ),
      );
    }
  }

  /**
   * Overrides FacetapiWidget::getDefaultSettings().
   */

  function getDefaultSettings() {
    return array(
      'soft_limit' => 20,
      'nofollow' => 1,
      'show_expanded' => 0,
      'show_expanded_narrow' => 0,
    );
  }
}

/**
 * Widget that renders facets as a list of clickable checkboxes.
 *
 * This widget renders facets in the same way as FacetapiWidgetLinks but uses
 * JavaScript to transform the links into checkboxes followed by the facet.
 */
class MediaFacetWidgetCheckboxLinks extends MediaFacetWidgetLinks {

  /**
   * Overrides FacetapiWidgetLinks::init().
   *
   * Adds additional JavaScript settings and CSS.
   */
  public function init() {
    parent::init();
    $this->jsSettings['makeCheckboxes'] = 1;
    drupal_add_css(drupal_get_path('module', 'facetapi') . '/facetapi.css');
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Sets the base class for checkbox facet items.
   */
  public function getItemClasses() {
    return array('facetapi-checkbox');
  }
}
