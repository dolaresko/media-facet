<?php

/**
 * @file
 * The mediafacet_images and mediafacet_checkbox_images widget plugin classes.
 */

/**
 * Widget that renders facets as a list of clickable links.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup.
 */
class MediaFacetWidgetImages extends MediaFacetWidgetLinks {
  /**
   * Transforms the render array for use with theme_item_list().
   *
   * The recursion allows this function to act on the various levels of a
   * hierarchical data set.
   *
   * @param array $build
   *   The items in the facet's render array being transformed.
   *
   * @return array
   *   The "items" parameter for theme_item_list().
   */
  function buildListItems($build) {
    $settings = $this->settings->settings;

    // Initializes links attributes, adds rel="nofollow" if configured.
    $attributes = ($settings['nofollow']) ? array('rel' => 'nofollow') : array();
    $attributes += array('class' => $this->getItemClasses());
    $count = $this->facet->getAdapter()->getResultCount();
    // Builds rows.
    $items = array();
    foreach ($build as $value => $item) {
      $row = array('class' => array());

      // Initializes variables passed to theme hook.
      $variables = array(
        'text' => $item['#markup'],
        'path' => $item['#path'],
        'count' => $item['#count'],
        'options' => array(
          'attributes' => $attributes,
          'html' => $item['#html'],
          'query' => $item['#query'],
        ),
      );

      // Adds the facetapi-zero-results class to items that have no results.
      if (!$item['#count']) {
        $variables['options']['attributes']['class'][] = 'facetapi-zero-results';
      }

      // If the item has no children, it is a leaf.
      if (empty($item['#item_children'])) {
        $row['class'][] = 'leaf';
      }
      else {
        // If the item is active or the "show_expanded" setting is selected,
        // show this item as expanded so we see its children.
        if ($item['#active'] || !empty($settings['show_expanded'])) {
          $row['class'][] = 'expanded';
          $row['children'] = $this->buildListItems($item['#item_children']);
        }
        elseif(!empty($settings['show_expanded_narrow']) && $item['#count'] == $count) {
          $row['class'][] = 'expanded';
          $row['children'] = $this->buildListItems($item['#item_children']);
        }
        else {
          $row['class'][] = 'collapsed';
        }
      }

      // Gets theme hook, adds last minute classes.
      $class = ($item['#active']) ? 'facetapi-active' : 'facetapi-inactive';
      $variables['options']['attributes']['class'][] = $class;

      // Themes the link, adds row to items.
      $mediafacet_output = '';
      if (isset($item['#indexed_value']) && !empty($item['#indexed_value'])) {
        $facet = $this->key . ':' . $item['#indexed_value'];
        $mediafacets = entity_load('mediafacet', FALSE, array('facet' => $facet));
        if (!empty($mediafacets)) {
          foreach ($mediafacets as $mediafacet) {
            if ($mediafacet->type == 'image' && isset($mediafacet->fid) && $file = file_load($mediafacet->fid)) {
              $path = image_style_url($settings[$this->id . '_style'], $file->uri);
              $mediafacet_output .= theme('image', array('path' => $path));
            }
          }
        }
      }

      if ($settings[$this->id . '_display_option'] == 'image') {
        $variables['text'] = $mediafacet_output;
      }
      elseif ($settings[$this->id . '_display_option'] == 'image_text') {
        $variables['text'] = $mediafacet_output . ' ' . $variables['text'];
      }
      elseif ($settings[$this->id . '_display_option'] == 'text_image') {
        $variables['text'] .= ' ' . $mediafacet_output;
      }

      $variables['options']['html'] = TRUE;
      $row['data'] = theme($item['#theme'], $variables);
      $items[] = $row;
    }

    return $items;
  }

  /**
   * Gets the base class array for a facet item.
   *
   * Classes that extend FacetapiWidgetLinks will often overide this method to
   * alter the link displays via CSS without having to touch the render array.
   *
   * @return array
   *   An array of classes.
   */
  function getItemClasses() {
    return array();
  }

  /**
   * Overrides FacetapiWidget::settingsForm().
   */
  function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);

    $display_options = array(
      'image' => t('Image only'),
      'image_text' => t('Image + Text'),
      'text_image' => t('Text + Image'),
    );

    $form['widget']['widget_settings']['links'][$this->id][$this->id . '_display_option'] = array(
      '#type' => 'select',
      '#title' => t('Display option'),
      '#default_value' => $this->settings->settings[$this->id . '_display_option'],
      '#description' => t('How to display images.'),
      '#options' => $display_options,
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );

    $styles = image_styles();
    $options = array();
    foreach ($styles as $style_name => $style_info) {
      $options[$style_name] = isset($style_info['name']) ? $style_info['name'] : $style_name;
    }
    $form['widget']['widget_settings']['links'][$this->id][$this->id . '_style'] = array(
      '#type' => 'select',
      '#title' => t('Image style'),
      '#default_value' => $this->settings->settings[$this->id . '_style'],
      '#description' => t('Apply chosen image style for MediaFacet Images.'),
      '#options' => $options,
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );
  }

  /**
   * Overrides FacetapiWidget::getDefaultSettings().
   */
  function getDefaultSettings() {
    $styles = image_styles();
    return array(
      'soft_limit' => 20,
      'nofollow' => 1,
      'show_expanded' => 0,
      'show_expanded_narrow' => 0,
      $this->id . '_style' => key($styles),
      $this->id . '_display_option' => '',
    );
  }
}

/**
 * Widget that renders facets as a list of clickable checkboxes.
 *
 * This widget renders facets in the same way as FacetapiWidgetLinks but uses
 * JavaScript to transform the links into checkboxes followed by the facet.
 */
class MediaFacetWidgetCheckboxImages extends MediaFacetWidgetImages {

  /**
   * Overrides FacetapiWidgetLinks::init().
   *
   * Adds additional JavaScript settings and CSS.
   */
  public function init() {
    parent::init();
    $this->jsSettings['makeCheckboxes'] = 1;
    drupal_add_css(drupal_get_path('module', 'facetapi') . '/facetapi.css');
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Sets the base class for checkbox facet items.
   */
  public function getItemClasses() {
    return array('facetapi-checkbox');
  }
}
